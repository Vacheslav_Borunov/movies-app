/* eslint-disable jsx-a11y/mouse-events-have-key-events */
import React, { FC, useCallback } from 'react';
import { Link } from 'react-router-dom'
import debounce from 'lodash.debounce';
import './styles/index.css'
import logoApp from '../../image/logo_app.png'
import iconUser from '../../image/icon_user.png'

interface Props {
  searchMovie: (value:string)=>void,
}

const HatBar: FC<Props> = ({ searchMovie }) => {
  const updateMovie = (value: string) => {
    searchMovie(value)
  }

  const debounceOnChange = useCallback(
    debounce(updateMovie, 300),
    [],
  )

  const changeMovieValue = (value: string) => {
    if (value.length > 3) {
      debounceOnChange(value)
    }
  }

  return (
    <header className="wrapper_header">
      <Link to="/" className="wrapper_logo">
        <img alt="logo_app" src={ logoApp } className="logo_app" />
        <p className="label_app">COCO</p>
      </Link>
      <div className="wrapper_nav_search">
        <nav className="wrapper_nav">
          <ul>
            <li><Link to="/films">Фильмы</Link></li>
            <li><Link to="/cartoon">Мультфильмы</Link></li>
            <li><Link to="/serials">Сериалы</Link></li>
          </ul>
        </nav>
        <div className="box">
          <form name="search">
            <Link to="/search">
              <input
                type="text"
                className="input"
                name="txt"
                onChange={ e => changeMovieValue(e.target.value) }
              />
            </Link>
          </form>
          <i className="fas fa-search" />
        </div>
      </div>
      <div className="wrapper_infoUser">
        <Link to="/userPage">
          <img alt="iconUser" src={ iconUser } className="icon_User" />
        </Link>
      </div>
    </header>
  )
}

export default HatBar;

import React, { FC, useEffect } from 'react';
import { Link } from 'react-router-dom'
import { observer } from 'mobx-react'
import './styles/index.css'

interface Props {
  id: number,
  poster: string,
  shortDescription: string,
  cardFavorites: {
    idFavorit: number,
    posterFavorit: string,
    shortDescriptionFavorit: string,
  }[],
  addFavorites: (id:number, poster: string, shortDescription:string)=>void,
  getAllInfoOneMove: (id:string)=>void,
}

const Card: FC<Props> = observer(({
  id,
  poster,
  shortDescription,
  cardFavorites,
  addFavorites,
  getAllInfoOneMove,
}) => {
  const renderBtnFavorites = () => (cardFavorites?.find(el => el.idFavorit === id) ? (
    <button onClick={ () => { addFavorites(id, poster, shortDescription) } } className="btn_favorit_on" />
  ) : (
    <button onClick={ () => { addFavorites(id, poster, shortDescription) } } className="btn_favorit" />
  ))

  return (
    <div className="card">
      <img alt="logo_app" src={ poster } className="poster" />
      <div className="content">
        <p className="copy">{shortDescription}</p>
        <div>
          <Link to="/moviePage">
            <button onClick={ () => { getAllInfoOneMove(`${id}`) } } className="btn_watch">Смотреть</button>
          </Link>
          {renderBtnFavorites()}
        </div>
      </div>
    </div>
  )
})

export default Card;

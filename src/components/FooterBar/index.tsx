import React from 'react';
import './styles/index.css'
import logoApp from '../../image/logo_app.png'

const FooterBar = () => (
  <footer className="wrapper_footer">
    <div className="wrapper_logo">
      <img alt="logo_app" src={ logoApp } className="logo_app" />
      <p className="label_app">COCO</p>
    </div>
  </footer>
)

export default FooterBar;

// @ts-nocheck
import axios from 'axios';
import { makeAutoObservable, toJS } from 'mobx';

import React from 'react';
import { TypeVideo } from '../type/enum';

const SRTTINGS = {
  GET_LIST_MOVE: 'https://api.kinopoisk.dev/v1/movie',
  KEY: '7FNWX9E-5AX4V0P-P1K5BP1-4ZS4JT4',
  ACCEPT: 'application/json',
}

const PLUG = {
  URL: 'https://i.ytimg.com/vi/lcYpaYrYpus/maxresdefault.jpg',
  POSTER: 'https://www.booksfer.com/uploads/images/202112/img_x500_61cc2948cedbc4-72338713-81333129.png',
  DISCRIPTION: 'Тут толькол матом)',
}

class Store {
  dataAllInfoMovie = []

  homeCardInfo = []

  filmsCardInfo = []

  cartoonCardInfo = []

  serialsCardInfo = []

  searchMovieCard = []

  cardFavorites = JSON.parse(localStorage.getItem('cardFavorites'))

  oneMovieInfo = {
    id: 0,
    movieName: '',
    year: 0,
    ageRating: 0,
    poster: '',
    description: '',
    shortDescription: '',
    treller: '',
  }

  constructor() {
    makeAutoObservable(this)
  }

  getCardInfo = arrayCard => {
    if (arrayCard.find(el => el.id === this.dataAllInfoMovie[0].id)) {
      return arrayCard
    }
    const itemCard = this.dataAllInfoMovie.map(el => ({
      id: el.id,
      poster: el.poster?.previewUrl || PLUG.POSTER,
      shortDescription: el?.shortDescription || PLUG.DISCRIPTION,
    }))
    return (arrayCard.push(...itemCard))
  }

  getAllInfoMove = async (currentPage:number) => {
    if (currentPage < 20) {
      this.dataAllInfoMovie = []
      await axios({
        url: `${SRTTINGS.GET_LIST_MOVE}`,
        params: { page: currentPage, limit: 35 },
        headers: {
          accept: `${SRTTINGS.ACCEPT}`,
          'X-API-KEY': `${SRTTINGS.KEY}`,
        },
      }).then(data => this.dataAllInfoMovie = data.data.docs)
      this.getCardInfo(this.homeCardInfo)
    }
  }

  getAllInfoMoveParams = async (filter:TypeVideo, currentPage:number) => {
    if (currentPage < 20) {
      this.dataAllInfoMovie = []
      await axios({
        url: `${SRTTINGS.GET_LIST_MOVE}`,
        params: { page: currentPage, limit: 35, type: filter },
        headers: {
          accept: `${SRTTINGS.ACCEPT}`,
          'X-API-KEY': `${SRTTINGS.KEY}`,
        },
      }).then(data => this.dataAllInfoMovie = data.data.docs)

      switch (filter) {
        case TypeVideo.Movie:
          this.getCardInfo(this.filmsCardInfo)
          break;
        case TypeVideo.TV_Series:
          this.getCardInfo(this.serialsCardInfo)
          break;
        case TypeVideo.Cartoon:
          this.getCardInfo(this.cartoonCardInfo)
          break;
        default:
      }
    }
  }

  getSortInfoOneMove = () => {
    const newTreller = this.oneMovieInfo.videos.trailers.find(el => el.site === 'youtube')

    this.oneMovieInfo = {
      id: this.oneMovieInfo.id,
      movieName: this.oneMovieInfo?.name || '',
      year: this.oneMovieInfo?.year || 1997,
      ageRating: this.oneMovieInfo.ageRating,
      poster: this.oneMovieInfo.poster?.previewUrl || PLUG.POSTER,
      description: this.oneMovieInfo?.description || PLUG.DISCRIPTION,
      shortDescription: this.oneMovieInfo?.shortDescription || PLUG.DISCRIPTION,
      treller: newTreller || PLUG.URL,
    }
  }

  getAllInfoOneMove = async (id:string) => {
    this.oneMovieInfo = {}
    await axios({
      url: `${SRTTINGS.GET_LIST_MOVE}/${id}`,
      headers: {
        accept: `${SRTTINGS.ACCEPT}`,
        'X-API-KEY': `${SRTTINGS.KEY}`,
      },
    }).then(data => this.oneMovieInfo = data.data)
    this.getSortInfoOneMove()
  }

  addFavorites = (id, poster, shortDescription) => {
    const checkId = !(this.cardFavorites.find((el => el.idFavorit === id)))
    if (checkId) {
      const itemFavoritesCard = {
        idFavorit: id,
        posterFavorit: poster || PLUG.POSTER,
        shortDescriptionFavorit: shortDescription || PLUG.DISCRIPTION,
      }
      this.cardFavorites.push(itemFavoritesCard)
      localStorage.setItem('cardFavorites', JSON.stringify(this.cardFavorites))
    } else {
      this.cardFavorites = this.cardFavorites.filter(el => el.idFavorit !== id)
      localStorage.setItem('cardFavorites', JSON.stringify(this.cardFavorites))
    }
  }

  searchMovie = async (value:string) => {
    this.searchMovieCard = []
    await axios({
      url: `${SRTTINGS.GET_LIST_MOVE}`,
      params: { page: 1, limit: 25, name: value },
      headers: {
        accept: `${SRTTINGS.ACCEPT}`,
        'X-API-KEY': `${SRTTINGS.KEY}`,
      },
    }).then(data => {
      this.searchMovieCard = data.data.docs.map(el => ({
        id: el.id,
        poster: el.poster?.previewUrl || PLUG.POSTER,
        shortDescription: el?.shortDescription || PLUG.DISCRIPTION,
      }))
    })
  }
}

export default new Store()

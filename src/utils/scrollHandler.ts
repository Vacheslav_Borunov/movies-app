export const scrollHandler = (event:any) => {
  const totalPageHeight = event.target.documentElement.scrollHeight;
  const visibleAreaPage = event.target.documentElement.scrollTop;
  const distanceFromTop = window.innerHeight;

  if (totalPageHeight - (visibleAreaPage + distanceFromTop) < 100) {
    return true
  }
  return false
}

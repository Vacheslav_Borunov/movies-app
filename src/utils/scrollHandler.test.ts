import { scrollHandler } from './scrollHandler'

const oldInnerHeight = global.window.innerHeight;

describe('block_test', () => {
  afterEach(() => {
    global.window.innerHeight = oldInnerHeight;
  })

  it('input_values', () => {
    global.window.innerHeight = 1080;

    const result = scrollHandler({

      currentTarget:
        {
          scrollHeight: 500,
          scrollTop: 400,
        },
    })

    expect(result).toEqual(true)
  })
})

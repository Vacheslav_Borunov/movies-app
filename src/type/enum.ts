export enum TypeVideo {
    Movie = 'movie',
    TV_Series = 'tv-series',
    Cartoon ='cartoon',
    Anime ='anime',
    Animated_Series ='animated-series',
    TV_Show = 'tv-show',
}

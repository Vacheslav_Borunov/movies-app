import React, { useEffect, useState } from 'react';
import { observer } from 'mobx-react'
import { Routes, Route } from 'react-router-dom'
import Home from './pages/Home/Home';
import './app.css';
import store from './store';
import HatBar from './components/HatBar';
import FooterBar from './components/FooterBar';
import Serials from './pages/Serials/Serials';
import Films from './pages/Films/Films';
import NotfoundPage from './pages/NotfoundPage/NotfoundPage';
import UserPage from './pages/UserPage/UserPage';
import MoviePage from './pages/MoviePage/MoviePage';
import Search from './pages/Search/Search';
import Cartoon from './pages/Cartoons/Cartoons';

const App = observer(() => (
  <div className="fon">
    <HatBar searchMovie={ store.searchMovie } />
    <Routes>
      <Route
        path="/"
        element={ (
          <Home
            getStore={ store.homeCardInfo }
            getAllInfoOneMove={ store.getAllInfoOneMove }
            addFavorites={ store.addFavorites }
            cardFavorites={ store.cardFavorites }
          />
        ) }
      />
      <Route
        path="/films"
        element={ (
          <Films
            getStore={ store.filmsCardInfo }
            getAllInfoOneMove={ store.getAllInfoOneMove }
            addFavorites={ store.addFavorites }
            cardFavorites={ store.cardFavorites }
          />
        ) }
      />
      <Route
        path="/serials"
        element={ (
          <Serials
            getStore={ store.serialsCardInfo }
            getAllInfoOneMove={ store.getAllInfoOneMove }
            addFavorites={ store.addFavorites }
            cardFavorites={ store.cardFavorites }
          />
        ) }
      />
      <Route
        path="/search"
        element={ (
          <Search
            getStore={ store.searchMovieCard }
            getAllInfoOneMove={ store.getAllInfoOneMove }
            addFavorites={ store.addFavorites }
            cardFavorites={ store.cardFavorites }
          />
        ) }
      />
      <Route
        path="/cartoon"
        element={ (
          <Cartoon
            getStore={ store.cartoonCardInfo }
            getAllInfoOneMove={ store.getAllInfoOneMove }
            addFavorites={ store.addFavorites }
            cardFavorites={ store.cardFavorites }
          />
        ) }
      />
      <Route
        path="/userPage"
        element={ (
          <UserPage
            cardFavorites={ store.cardFavorites }
            getAllInfoOneMove={ store.getAllInfoOneMove }
            addFavorites={ store.addFavorites }
          />
        ) }
      />
      <Route
        path="/moviePage"
        element={ (
          <MoviePage
            oneMovieInfo={ store.oneMovieInfo }
            getAllInfoOneMove={ store.getAllInfoOneMove }
            addFavorites={ store.addFavorites }
            cardFavorites={ store.cardFavorites }
          />
        ) }
      />
      <Route path="*" element={ <NotfoundPage /> } />
    </Routes>
    <FooterBar />
  </div>
))

export default App;

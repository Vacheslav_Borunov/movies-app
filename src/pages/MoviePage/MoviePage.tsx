import React, { FC, useEffect, useState } from 'react';
import PropTypes, { bool, number } from 'prop-types';
import './styles/index.css'
import { observer } from 'mobx-react'
import { RingSpinner } from 'react-spinners-kit';

interface Props {
    oneMovieInfo: {
      id: number,
      movieName: string,
      year: number,
      ageRating: number,
      poster: string,
      description: string,
      shortDescription: string,
      treller: string,
    },
    cardFavorites: {
      idFavorit: number,
      posterFavorit: string,
      shortDescriptionFavorit: string,
    }[],
    addFavorites: (id:number, poster: string, shortDescription:string)=>void,
    getAllInfoOneMove: (id:string)=>void,
  }

const LABELS = {
  MOVIE_CARD: 'Карточка фильма',
  WATCH_MOVIE: 'Смотреть фильм',
  WATCH_TRAILER: 'Смотреть трейлер',
}

const MoviePage: FC<Props> = observer(({
  oneMovieInfo: {
    id,
    movieName,
    year,
    ageRating,
    poster,
    description,
    shortDescription,
    treller,
  },
  cardFavorites,
  getAllInfoOneMove,
  addFavorites,
}) => {
  const [ flagVideo, setFlagVideo ] = useState<boolean>(false)

  useEffect(() => {
    if (id === 0) {
      const movieId = localStorage.getItem('movieId') === null ? '535341' : localStorage.getItem('movieId')
      getAllInfoOneMove(`${movieId}`)
    } else {
      localStorage.setItem('movieId', `${id}`);
    }
  }, [])

  const renderBtnFavorites = () => (cardFavorites?.find(el => el.idFavorit === id) ? (
    <button onClick={ () => { addFavorites(id, poster, shortDescription) } } className="favorites_on" />
  ) : (
    <button onClick={ () => { addFavorites(id, poster, shortDescription) } } className="favorites" />
  ))

  if (id === 0) {
    return (
      <div>
        <section className="wrapper_moveList">
          <p className="title_all_move">{LABELS.MOVIE_CARD}</p>
          <div className="wrapper_posters">
            <RingSpinner
              size={ 45 }
              color="#fff"
            />
          </div>
        </section>
      </div>
    );
  }

  const renderVideo = () => {
    if (flagVideo) {
      return (
        <embed src={ treller } className="wrapper_video" />
      )
    }
    return null
  }

  return (
    <div>
      <section className="wrapper_move_page">
        <p className="title_move_page">{LABELS.MOVIE_CARD}</p>
        <div className="wrapper_content">
          <img alt="move_logo" src={ poster } className="wrapper_movie_logo" />
          <div className="wrapper_movie_text">
            <p className="wrapper_title_movie">
              {movieName}
              &nbsp;
              (
              {year}
              )
              &ensp;
              {ageRating}
              +
            </p>
            <p className="wrapper_description_movie">
              {description}
            </p>
            <div className="wrapper_btn">
              <button className="btn_move_page">{LABELS.WATCH_MOVIE}</button>
              <button onClick={ () => { setFlagVideo(true) } } className="btn_move_page">{LABELS.WATCH_TRAILER}</button>
              {renderBtnFavorites()}
            </div>
            {renderVideo()}
          </div>
        </div>
      </section>
    </div>
  )
})

export default MoviePage;

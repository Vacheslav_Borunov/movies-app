import React, { FC } from 'react';
import PropTypes from 'prop-types';
import './styles/index.css'
import frontDropped from '../../image/front_dropped.jpg'

const NotfoundPage: FC = () => (
  <div>
    <section className="wrapper_list_error">
      <div className="wrapper_error">
        <strong className="title_front_dropped">404</strong>
        <strong className="title_front_dropped">Страница не найдена!</strong>
        <img alt="frontDropped" src={ frontDropped } className="front_dropped" />
      </div>
    </section>
  </div>
)

export default NotfoundPage;

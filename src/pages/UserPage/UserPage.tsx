import React, { FC, useEffect, useState } from 'react';
import PropTypes, { number } from 'prop-types';
import './styles/index.css'
import { observer } from 'mobx-react';
import Card from '../../components/Card';
import userLogo from '../../image/icon_user.png'

interface Props {
  cardFavorites: {
    idFavorit: number,
    posterFavorit: string,
    shortDescriptionFavorit: string,
  }[],
  addFavorites: (id:number, poster: string, shortDescription:string)=>void,
  getAllInfoOneMove: (id:string)=>void,
}

const LABELS = {
  TITLE_USER_PAGE: 'Личная страница',
  NAME_USER: 'User',
  GEN_USER: 'муж',
  AGE_USER: '27 лет',
  MY_LIBRARY: 'Моя библиотека',
}

const UserPage: FC<Props> = ({ cardFavorites, addFavorites, getAllInfoOneMove }) => {
  const renderCard = () => (
    cardFavorites?.map(el => (
      <Card
        key={ el.idFavorit }
        id={ el.idFavorit }
        poster={ el.posterFavorit }
        shortDescription={ el.shortDescriptionFavorit }
        getAllInfoOneMove={ getAllInfoOneMove }
        cardFavorites={ cardFavorites }
        addFavorites={ addFavorites }
      />
    ))
  )

  return (
    <div>
      <section className="wrapper_usesr_page">
        <p className="title_usesr_page">{LABELS.TITLE_USER_PAGE}</p>
        <div className="wrapper_user_card">
          <div className="wrapper_user_info">
            <img alt="user_logo" src={ userLogo } className="wrapper_user_logo" />
            <div className="text_info_user">
              <p className="title_name_user">{LABELS.NAME_USER}</p>
              <p className="title_name_user">{LABELS.GEN_USER}</p>
              <p className="title_name_user">{LABELS.AGE_USER}</p>
            </div>
          </div>
          <div className="wrapper_favorites_card">
            <p className="title_favorites_card">{LABELS.MY_LIBRARY}</p>
            <div className="wrapper_render_card">
              {renderCard()}
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default UserPage;

import React, { FC, useEffect, useState } from 'react';
import PropTypes, { number } from 'prop-types';
import './styles/index.css'
import { observer } from 'mobx-react';
import { RingSpinner } from 'react-spinners-kit';
import Card from '../../components/Card';
import store from '../../store';
import { TypeVideo } from '../../type/enum';
import { scrollHandler } from '../../utils/scrollHandler';

interface Props {
  getStore: {
    id: number,
    poster: string,
    shortDescription: string,
  }[],
  cardFavorites:[],
  addFavorites: (id:number, poster: string, shortDescription:string)=>void,
  getAllInfoOneMove: (id:string)=>void,
}

const filter:TypeVideo = TypeVideo.Cartoon

const LABELS = {
  TITLE_CARTOON: 'Мультфильмы',
}

const Cartoon: FC<Props> = ({
  getStore,
  cardFavorites,
  addFavorites,
  getAllInfoOneMove,
}) => {
  const [ currentPage, setCurrentPage ] = useState<number>(1)
  const [ movieLoadStatus, setMovieLoadStatus ] = useState<boolean>(false)

  useEffect(() => {
    store.getAllInfoMoveParams(filter, currentPage)
      .then(() => { setCurrentPage(currentPage + 1) })
      .finally(() => { setMovieLoadStatus(false) })
  }, [ movieLoadStatus ]);

  const getScrollHandler = (event:any) => {
    setMovieLoadStatus(scrollHandler(event))
  }

  useEffect(() => {
    document.addEventListener('scroll', getScrollHandler)
    return () => { document.removeEventListener('scroll', getScrollHandler) }
  }, [])

  if (getStore.length === 0) {
    return (
      <div>
        <section className="wrapper_moveList">
          <p className="title_all_move">{LABELS.TITLE_CARTOON}</p>
          <div className="wrapper_posters">
            <RingSpinner
              size={ 45 }
              color="#fff"
            />
          </div>
        </section>
      </div>
    );
  }

  const renderCard = () => (
    getStore.map(el => (
      <Card
        key={ el.id }
        id={ el.id }
        poster={ el.poster }
        shortDescription={ el.shortDescription }
        getAllInfoOneMove={ getAllInfoOneMove }
        cardFavorites={ cardFavorites }
        addFavorites={ addFavorites }
      />
    ))
  )

  return (
    <div>
      <section className="wrapper_moveList">
        <p className="title_all_move">{LABELS.TITLE_CARTOON}</p>
        <div className="wrapper_posters">
          {renderCard()}
        </div>
      </section>
    </div>
  );
}

export default Cartoon;

import React, { FC, useEffect, useState } from 'react';
import PropTypes, { number } from 'prop-types';
import './styles/index.css'
import { observer } from 'mobx-react'
import { RingSpinner } from 'react-spinners-kit';
import { toJS } from 'mobx';
import Card from '../../components/Card';

interface Props {
    getStore: {
    id: number,
    poster: string,
    shortDescription: string,
  }[],
  cardFavorites:[],
  addFavorites: (id:number, poster: string, shortDescription:string)=>void,
  getAllInfoOneMove: (id:string)=>void,
}

const LABELS = {
  TITLE_SEARCH: 'Поиск',
}

const Search: FC<Props> = observer(({
  getStore,
  cardFavorites,
  addFavorites,
  getAllInfoOneMove,
}) => {
  if (getStore.length === 0) {
    return (
      <div>
        <section className="wrapper_moveList">
          <p className="title_all_move">{LABELS.TITLE_SEARCH}</p>
          <div className="wrapper_posters">
            <RingSpinner
              size={ 45 }
              color="#fff"
            />
          </div>
        </section>
      </div>
    );
  }

  const renderCard = () => (
    getStore.map(el => (
      <Card
        key={ el.id }
        id={ el.id }
        poster={ el.poster }
        shortDescription={ el.shortDescription }
        getAllInfoOneMove={ getAllInfoOneMove }
        cardFavorites={ cardFavorites }
        addFavorites={ addFavorites }
      />
    ))
  )

  return (
    <div>
      <section className="wrapper_moveList">
        <p className="title_all_move">{LABELS.TITLE_SEARCH}</p>
        <div className="wrapper_posters">
          {renderCard()}
        </div>
      </section>
    </div>
  );
})

export default Search;
